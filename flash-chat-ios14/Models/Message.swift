//
//  Message.swift
//  flash-chat-ios14
//
//  Created by tran tu on 22/05/2021.
//  Copyright © 2021 Tutn. All rights reserved.
//

import Foundation

struct Message {
    let sender: String
    let body: String
    
}
